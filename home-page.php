<?php /* Template Name: Homepage */ get_header(); ?>

<!-- Hero section  -->
<?php if ( have_rows( 'home_hero_content' ) ) : ?>

<section class="hero hero--curve-tablet">
    <div class="container is-relative">
        <div class="columns">
            <?php while ( have_rows( 'home_hero_content' ) ) : the_row(); ?>

            <div class="column hero__content hero--curve-mobile">
                <div class="hero__content-inner">
                    <?php the_sub_field( 'home_hero_title' ); ?>
                    <?php the_sub_field( 'home_hero_paragraph' ); ?>
                    <?php if ( have_rows( 'button_group' ) ) : ?>
                    <div class="btn-group">
                        <?php while ( have_rows( 'button_group' ) ) : the_row(); ?>
                        <?php $button_1 = get_sub_field( 'button_1' ); ?>
                        <?php if ( $button_1 ) { ?>
                        <a href="<?php echo $button_1['url']; ?>" target="<?php echo $button_1['target']; ?>"
                            class="btn btn--white"><?php echo $button_1['title']; ?></a>
                        <?php } ?>
                        <?php $button_2 = get_sub_field( 'button_2' ); ?>
                        <?php if ( $button_2 ) { ?>
                        <a href="<?php echo $button_2['url']; ?>" class="btn btn--border btn--border-white"
                            target="<?php echo $button_2['target']; ?>"><?php echo $button_2['title']; ?></a>
                        <?php } ?>
                        <?php endwhile; ?>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            <?php endwhile; ?>

            <?php $home_hero_image = get_field( 'home_hero_image' ); ?>
            <?php if ( $home_hero_image ) { ?>
            <div class="column hero__img background-yellow">
                <div class="hero__img-inner">
                    <!-- <img src="<?php //echo $home_hero_image['url']; ?>" alt="<?php //echo $home_hero_image['alt']; ?>" /> -->
                    <img src="<?php echo get_template_directory_uri() ?>/img/hero-img-bg.svg" alt="">

                    <div data-aos="fade-down-right" data-aos-delay="1700"
                        class="circle-yellow shape-circle-yellow animation--anti-clockwise2"></div>

                    <div data-aos="fade-down-right" data-aos-delay="1500"
                        class="circle-blue shape-circle-blue bobble--animation"></div>

                    <div data-aos="fade-down-left" 
                        class="circle-red shape-circle-red animation--clockwise"></div>

                    <img class="question"
                        src="<?php echo get_template_directory_uri() ?>/img/hero-questionmark.png" alt="">

                    <div class="blue-lines">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <div class="yellow-box-list">
                        <img data-aos="fade" src="<?php echo get_template_directory_uri() ?>/img/hero-list-empty.png" alt="">
                        <img class="checkmark" src="<?php echo get_template_directory_uri() ?>/img/hero-check-mark.png" alt="">
                        <img class="checkmark" src="<?php echo get_template_directory_uri() ?>/img/hero-check-mark.png" alt="">
                        <img  class="checkmark" src="<?php echo get_template_directory_uri() ?>/img/hero-check-mark.png" alt="">
                        <img  class="checkmark" src="<?php echo get_template_directory_uri() ?>/img/hero-check-mark.png" alt="">
                    </div>
                    <div data-aos="zoom-in" class="yellow-border-box ">
                        <img class="box" src="<?php echo get_template_directory_uri() ?>/img/hero-single-people.png" alt="">
                    </div>

                    <div data-aos="zoom-in" class="red-border-box animation--fade">
                        <img class="box" src="<?php echo get_template_directory_uri() ?>/img/hero-red-box.png" alt="">
                        <img data-aos="fade" data-aos-delay="500" class="people"
                            src="<?php echo get_template_directory_uri() ?>/img/hero-people-red.png" alt="">
                    </div>
                    <!-- data-aos="rotate" data-aos-duration="1800" -->
                    <img  class="ball"
                        src="<?php echo get_template_directory_uri() ?>/img/hero-ball.png" alt="">
                    <img data-aos="fade" class="heart pulse1" src="<?php echo get_template_directory_uri() ?>/img/hero-heart.png"
                        alt="">

                </div>
                <?php } ?>

            </div>
        </div>
</section>

<?php endif; ?>
<!-- End hero section  -->

<!-- About use section  -->
<section class="container about-section section">
    <div class="section-title section-title--center">
        <?php the_field( 'section_title' ); ?>
    </div>
    <?php if ( have_rows( 'about_us_-_box' ) ) : ?>
    <div class="columns is-variable is-4 is-multiline">
        <?php while ( have_rows( 'about_us_-_box' ) ) : the_row(); ?>
        <?php $post_object = get_sub_field( 'add_page_box' ); ?>
        <?php if ( $post_object ): ?>
        <?php $post = $post_object; ?>
        <?php setup_postdata( $post ); ?>
        <div class="column is-6-tablet is-3-desktop cart-icon-wrap">
            <a href="<?php the_permalink(); ?>" class="cart-icon cart-icon--center">

                <?php $box_icon = get_sub_field( 'box_icon' ); ?>
                <?php if ( $box_icon ) { ?>
                <div class="cart-icon__icon-img">
                    <img src="<?php echo $box_icon['url']; ?>" alt="<?php echo $box_icon['alt']; ?>" />
                </div>
                <?php } ?>

                <div class="cart-icon__content">
                    <h4><?php the_title(); ?></h4>

                    <?php if(get_sub_field( 'add_custom_excerpt' )) { ?>
                    <?php the_sub_field( 'add_custom_excerpt' ); ?>
                    <?php
                                }else {
                                ?>
                    <?php the_excerpt(); ?>
                    <?php
                                } ?>
                </div>
                <button class="btn-link">Learn more <span class="arrow arrow--right"></span></button>
            </a>
        </div>
        <?php wp_reset_postdata(); ?>
        <?php endif; ?>
        <?php endwhile; ?>
    </div>
    <?php endif; ?>
</section>
<!-- End about us section  -->

<!-- Float section  -->
<section class="section background-yellow float-section float-section--move-box">
    <div class="shape-group">
        <div data-aos="fade-right" data-aos-delay="200" class="shape-circle-red">
            <img src="<?php echo get_template_directory_uri() ?>/img/red-shape-half-red-left.png" alt="">
        </div>
        <div data-aos="zoom-in" data-aos-delay="700" class="shape-circle-white">
            <img src="<?php echo get_template_directory_uri() ?>/img/white-shape-half.png" alt="">
        </div>
    </div>

    <div data-aos="fade" data-aos-duration="1500" data-aos-delay="600" class="shape-big-center">
        <img src="<?php echo get_template_directory_uri() ?>/img/yellow-shape-half-big.png" alt="">
    </div>

    <div class="shape-group-right">
        <div data-aos="fade-left" class="shape-circle-red">
            <img src="<?php echo get_template_directory_uri() ?>/img/red-shape-half-red-right.png" alt="">
        </div>
        <div data-aos="fade" data-aos-delay="1000" class="circle-yellow shape-circle-yellow bobble--animation">
        </div>
        <div data-aos="fade-up"  data-aos-delay="500" class="shape-circle-blue">
            <img src="<?php echo get_template_directory_uri() ?>/img/blue-shape-half.png" alt="">
        </div>
    </div>

    <div class="container">
        <div class="columns is-multiline">
            <div data-aos="fade_down"
                class="column is-12-tablet is-4-desktop is-5-widescreen float-section--white-font is-flex is-flex-direction-column is-justify-content-center">
                <?php the_field( 'title' ); ?>
            </div>
            <!-- Float box 1 -->
            <?php if ( have_rows( 'float_box_1' ) ) : ?>
            <div class="column is-10-tablet is-8-desktop is-7-widescreen is-offset-2-tablet is-offset-0-desktop">

                <?php while ( have_rows( 'float_box_1' ) ) : the_row(); ?>
                <?php $float_box_link = get_sub_field( 'float_box_link' ); ?>
                <?php if ( $float_box_link ) { ?>
                <?php $float_box_url  = $float_box_link['url']; ?>
                <?php } ?>

                <a data-aos="fade" href="<?php echo $float_box_url; ?>" class="float-box tilt">

                    <?php $float_box_icon = get_sub_field( 'float_box_icon' ); ?>
                    <?php if ( $float_box_icon ) { ?>
                    <div class="float-box__icon">
                        <img src="<?php echo $float_box_icon['url']; ?>" alt="<?php echo $float_box_icon['alt']; ?>" />
                    </div>
                    <?php } ?>

                    <div class="float-box__content">
                        <h3 class="float-box__title"> <?php the_sub_field( 'float_box_title' ); ?></h3>
                        <h6 class="float-box__subtitle"> <?php the_sub_field( 'float_box_subtitle' ); ?></h6>
                        <p><?php the_sub_field( 'flot_box_description' ); ?></p>

                        <?php if ( $float_box_link ) { ?>
                        <button class="btn-link"><?php echo $float_box_link['title']; ?><span
                                class="arrow arrow--right"></span></button>
                        <?php } ?>
                    </div>

                </a>

                <?php endwhile; ?>

            </div>
            <?php endif; ?>
        </div>

        <!-- Float box 2 -->
        <?php if ( have_rows( 'float_box_2' ) ) : ?>
        <div class="columns is-centered">
            <?php while ( have_rows( 'float_box_2' ) ) : the_row(); ?>
            <?php $float_box_link = get_sub_field( 'float_box_link' ); ?>
            <?php if ( $float_box_link ) { ?>
            <?php  $flaot_box_link = $float_box_link['url']; ?>
            <?php } else { ?>
            <?php $flaot_box_link = "#";
                    }?>

            <div class="column is-10-tablet is-8-desktop is-7-widescreen">
                <a data-aos="fade" href="<?php echo $flaot_box_link; ?>" class="float-box tilt">

                    <?php $float_box_icon2 = get_sub_field( 'float_box_icon2' ); ?>
                    <?php if ( $float_box_icon2 ) { ?>
                        <div class="float-box__icon">
                            <img src="<?php echo $float_box_icon2['url']; ?>" alt="<?php echo $float_box_icon2['alt']; ?>" />
                        </div>
                    <?php } ?>

                    <div class="float-box__content">
                        <h3 class="float-box__title"><?php the_sub_field( 'float_box_title' ); ?></h3>
                        <h6 class="float-box__subtitle"><?php the_sub_field( 'float_box_subtitle' ); ?></h6>
                        <p><?php the_sub_field( 'flot_box_description' ); ?></p>
                        <?php if ( $float_box_link ) { ?>
                        <button class="btn-link"><?php echo $float_box_link['title']; ?><span
                                class="arrow arrow--right"></span></button>
                        <?php } ?>

                    </div>
                </a>
            </div>
            <?php endwhile; ?>
        </div>
        <?php endif; ?>

        <!-- Float box 3  -->

        <?php if ( have_rows( 'float_box_3' ) ) : ?>
        <div class="columns">
            <?php while ( have_rows( 'float_box_3' ) ) : the_row(); ?>
            <?php $float_box_link = get_sub_field( 'float_box_link' ); ?>
            <?php if ( $float_box_link ) { ?>
            <?php $float_box_link = $float_box_link['url']; ?>
            <?php } else {
                        $float_box_link = "#";
                    } ?>
            <div class="column is-10-tablet is-8-desktop is-7-widescreen">
                <a data-aos="fade" href="<?php echo $float_box_link; ?>" class="float-box tilt">

                    <?php $float_box_icon = get_sub_field( 'float_box_icon' ); ?>
                    <?php if ( $float_box_icon ) { ?>
                    <div class="float-box__icon">
                        <img src="<?php echo $float_box_icon['url']; ?>" alt="<?php echo $float_box_icon['alt']; ?>" />
                    </div>
                    <?php } ?>

                    <div class="float-box__content">
                        <h3 class="float-box__title"><?php the_sub_field( 'float_box_title' ); ?></h3>
                        <h6 class="float-box__subtitle"><?php the_sub_field( 'float_box_subtitle' ); ?></h6>
                        <p><?php the_sub_field( 'flot_box_description' ); ?></p>
                        <?php $float_box_link = get_sub_field( 'float_box_link' ); ?>
                        <?php if ( $float_box_link ) { ?>
                        <button class="btn-link"><?php echo $float_box_link['title']; ?><span
                                class="arrow arrow--right"></span></button>
                        <?php } ?>
                    </div>
                </a>
            </div>
            <?php endwhile; ?>
        </div>
        <?php endif; ?>
    </div>
</section>
<!-- End float section  -->

<!-- News section  -->
<section class="news-section section">
    <div data-aos="fade-right" class="shape-gray-left">
        <img src="<?php echo get_template_directory_uri() ?>/img/gray-shape-half-left.png" alt="">
    </div>

    <div class="container">
        <div class="section-title">
            <h2>News</h2>
        </div>
        <div class="columns is-desktop is-variable is-7 news-section--remove-padding">
            <!-- Display first post  -->
            <?php 
         $args = array(
            'post_type' => 'post',
            'posts_per_page' => 1
         ) ?>
            <?php $query = new WP_query($args); ?>
            <?php if($query->have_posts()) { ?>
            <?php while($query->have_posts()) : $query->the_post(); ?>
            <div class="column is-6-desktop">
                <a href="<?php the_permalink(); ?>" class="cart-news cart-news--large">
                    <!-- Get featured image  -->
                    <?php 
                     if ( has_post_thumbnail() ) {
                        $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );
                        $backgroundImg = $backgroundImg[0];
                     }else {
                        $backgroundImg = "";
                     }
                     ?>
                    <div class="cart-news__img cart-news-row__img--square cover"
                        style="background-image: url(<?php echo $backgroundImg; ?>)">
                    </div>
                    <div class="cart-news__content">
                        <span class="cart-news__date"><?php echo get_the_date(); ?></span>
                        <h3 class="cart-news__title"><?php the_title(); ?></h3>
                        <?php the_excerpt(); ?>
                        <button class="btn-link">Read more <span class="arrow arrow--right"></span></button>
                    </div>
                </a>
            </div>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
            <?php
         } ?>

            <?php 
         $args = array(
            'post_type' => 'post',
            'offset' => 1,
            'posts_per_page' => 2
         ) ?>

            <?php $query = new WP_query($args); ?>
            <?php if($query->have_posts()) { ?>
            <div class="column is-6-desktop news-section__2rows">
                <?php $i = 1; ?>
                <?php while($query->have_posts()) : $query->the_post(); ?>
                <a href="<?php the_permalink(); ?>" class="cart-news">
                    <?php 
                  if ( has_post_thumbnail() ) {
                     $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );
                     $backgroundImg = $backgroundImg[0];
                  }else {
                     $backgroundImg = "";
                  }
                  ?>
                    <div class="cart-news__img cart-news-row__img--square cover"
                        style="background-image: url(<?php echo $backgroundImg; ?>)">
                    </div>
                    <div class="cart-news__content">
                        <span class="cart-news__date"><?php echo get_the_date(); ?></span>
                        <h4 class="cart-news__title"><?php the_title(); ?></h4>
                        <button href="#" class="btn-link">Read more <span class="arrow arrow--right"></span></button>
                    </div>
                </a>
                <?php if($i == 1) {
                  echo "<hr>";
               }else ?>
                <?php $i++; ?>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            </div>
            <?php } ?>
        </div>

        <div class="columns btn-section ">
            <div class="column is-flex is-justify-content-center">
                <a href="/about/news/" class="btn btn--red btn-center">Read all news</a>
            </div>
        </div>

    </div>
</section>
<!-- End news section  -->



<?php get_footer(); ?>