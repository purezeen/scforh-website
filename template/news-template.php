<?php /* Template Name: News */ get_header(); ?>

<?php
    $post_ID = get_the_ID();
    $parentpost_id = wp_get_post_parent_id( $post_ID );
?>

<?php if ( get_field( 'sidebar_layout' , $parentpost_id ) == 1 ) {  ?>
   <?php if ($post->post_parent)  {
    $ancestors=get_post_ancestors($post->ID);
    $root=count($ancestors)-1;
    $parent = $ancestors[$root];
} else {
    $parent = $post->ID;
} ?>

<div class="layout about-page">
    <div class="container">
        <div class="columns is-multiline is-desktop">
            <aside class="column is-4-desktop layout__sidebar">
                <div class="layout__sidebar--background"></div>
                <div class="layout__sidebar__nav-list">
                    <div class="layout__sidebar__nav-list-inner">
                        
                        <ul class="accordion__sidebar unstyle-list">
                            <?php //this will be where you add your statement if we're on a parent/child page
                            wp_list_pages(array(
                                'title_li' => '',
                                'child_of' => $parent,
                                'walker' => new BS_Page_Walker(),
                            ));
                            ?>
                        </ul>

                    </div>
                </div>
                <div class="layout__sidebar__nav-tab">
                    <h3>About</h3>
                </div>
            </aside>

            <main class="column is-8-desktop layout__main">
                <h1 class="page-title">News</h1>
                <?php get_template_part( 'template-parts/post', 'list' ); ?>
            </main>
        </div>
    </div>
</div>

<?php get_footer(); ?>
<?php

} else { ?>
	<div class="layout layout__full-width news-page">

    <div class="shape-group">
        <div class="shape-circle-red">
            <img src="<?php echo get_template_directory_uri() ?>/img/red-shape-half-red-left.png" alt="">
        </div>
        <div class="shape-circle-white">
            <img src="<?php echo get_template_directory_uri() ?>/img/white-shape-half.png" alt="">
        </div>
        <div class="shape-circle-blue">
            <img src="<?php echo get_template_directory_uri() ?>/img/blue-circle-small2.png" alt="">
        </div>
    </div>

    <div class="shape-big-center">
        <img src="<?php echo get_template_directory_uri() ?>/img/yellow-shape-half-big.png" alt="">
    </div>

    <div class="shape-group-blue-right">
        <div class="shape-circle-blue">
            <img src="<?php echo get_template_directory_uri() ?>/img/blue-shape-half-right.png" alt="">
        </div>
        <div class="shape-circle-white">
            <img src="<?php echo get_template_directory_uri() ?>/img/white-circle-small.png" alt="">
        </div>
    </div>

    <div class="shape-gray-right">
        <img src="<?php echo get_template_directory_uri() ?>/img/gray-shape-half-right.png" alt="">
    </div>

      <div class="container">
         <main class="layout__inner">
            <div class="news-page__content">
               <h1 class="page-title">News</h1>
                <?php get_template_part( 'template-parts/post', 'list' ); ?>
            </div>
         </main>
      </div>
   </div>

    <?php get_footer('white'); ?>

    <?php
    
} ?>

