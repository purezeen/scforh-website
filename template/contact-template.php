
<?php /* Template Name: Contact */ get_header(); ?>


<section class="contact">
<div class="shape-group">
        <div class="shape-circle-red">
            <img src="<?php echo get_template_directory_uri() ?>/img/red-shape-half-red-left.png" alt="">
        </div>
        <div class="shape-circle-white">
            <img src="<?php echo get_template_directory_uri() ?>/img/white-shape-half.png" alt="">
        </div>
        <div class="shape-circle-blue">
            <img src="<?php echo get_template_directory_uri() ?>/img/blue-circle-small2.png" alt="">
        </div>
    </div>
    <div class="shape-group-right">
        <div class="shape-circle-red">
            <img src="<?php echo get_template_directory_uri() ?>/img/red-shape-half-red-right.png" alt="">
        </div>
        <div class="shape-circle-white">
            <img src="<?php echo get_template_directory_uri() ?>/img/white-circle-small.png" alt="">
        </div>
        <div class="shape-circle-blue">
            <img src="<?php echo get_template_directory_uri() ?>/img/blue-shape-half.png" alt="">
        </div>
    </div>

   <div class="container">
      <div class="columns ">
         <div class="column is-7-tablet is-6-desktop background-blue">
            <div class="contact__inner contact--font-white">

            <?php if ( have_rows( 'contact_information' ) ) : ?>
                <?php while ( have_rows( 'contact_information' ) ) : the_row(); ?>
                    <?php $icon = get_sub_field( 'icon' ); ?>
                    <?php if ( $icon ) { ?>
                        <div class="icon-medium">
                            <img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>" />
                        </div>
                    <?php } ?>
                    <?php the_sub_field( 'description' ); ?>
                    <h5><?php the_sub_field( 'form_title' ); ?></h5>
                    <?php if(get_sub_field( 'add_form' )): ?>
                        <?php $form_id = get_sub_field( 'add_form' ); ?>
                        <?php gravity_form( $form_id ,  $display_title = true, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, $echo = true ); ?>
                    <?php endif; ?>

                <?php endwhile; ?>
            <?php endif; ?>
            </div>
         </div>
         <div class="column is-5-tablet is-6-desktop background-yellow">
            <div class="contact__inner contact__ambassadors">

            <?php if ( have_rows( 'ambassadors_information' ) ) : ?>
                <?php while ( have_rows( 'ambassadors_information' ) ) : the_row(); ?>
                    <?php the_sub_field( 'description' ); ?>
                    <?php $button = get_sub_field( 'button' ); ?>
                    <?php if ( $button ) { ?>
                        <a href="<?php echo $button['url']; ?>" class="btn btn--border btn--border-blue" target="<?php echo $button['target']; ?>"><?php echo $button['title']; ?></a>
                    <?php } ?>
                <?php endwhile; ?>
            <?php endif; ?>
            </div>
         </div>
      </div>
   </div>
</section>

<?php get_footer(); ?>

