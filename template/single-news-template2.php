<?php /* Template Name: Single news 2 */ get_header(); ?>


<div class="layout layout__full-width news-page">

    <div class="shape-group">
        <div class="shape-circle-red">
            <img src="<?php echo get_template_directory_uri() ?>/img/red-shape-half-red-left.png" alt="">
        </div>
        <div class="shape-circle-white">
            <img src="<?php echo get_template_directory_uri() ?>/img/white-shape-half.png" alt="">
        </div>
        <div class="shape-circle-blue">
            <img src="<?php echo get_template_directory_uri() ?>/img/blue-circle-small2.png" alt="">
        </div>
    </div>

    <div class="shape-big-center">
        <img src="<?php echo get_template_directory_uri() ?>/img/yellow-shape-half-big.png" alt="">
    </div>

    <div class="shape-group-blue-right">
        <div class="shape-circle-blue">
            <img src="<?php echo get_template_directory_uri() ?>/img/blue-shape-half-right.png" alt="">
        </div>
        <div class="shape-circle-white">
            <img src="<?php echo get_template_directory_uri() ?>/img/white-circle-small.png" alt="">
        </div>
    </div>

    <div class="shape-gray-right">
        <img src="<?php echo get_template_directory_uri() ?>/img/gray-shape-half-right.png" alt="">
    </div>

    <div class="container">

        <main class="layout__inner">
            <div class="news-page__content with-heroimg">
                <div class="hero-img cover"
                    style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/news1.png)">
                </div>
                <!-- <img class="hero-img" src="<?php echo get_template_directory_uri() ?>/img/news1.png" alt="SCforH"> -->
                <h3>Sports Clubs for Health chosen as an Erasmus+ Success Story</h3>
                <p>The third international SCforH project entitled Creating Mechanisms for Continuous Implementation of
                    the Sports Club for Health Guidelines in the European Union (SCforH 2020-22) has been launched in
                    February 2020. This 3-year long project
                    is funded by a European Union Erasmus+ Collaborative Partnerships grant of 398.845 Euro, and it
                    includes 17 partner institutions from 13 countries. </p>
                <blockquote>SCforH is an expert-based approach that supports clubs as well as national and regional
                    sport organisations to recognize the health potential of their sports disciplines and organize
                    health-enhancing sports activities in the sports club
                    setting.
                </blockquote>
                <p>Achieving good health is a desirable goal of sports clubs members and society as a whole, regardless
                    of the reasons an individual engages in sports. SCforH encourages sports clubs to review their own
                    activities and sport discipline(s)
                    in the light of their health potential and promote greater participation in health-enhancing sports
                    activities.
                </p>
                <h6>The SCforH approach is based on the following ideas:</h6>
                <ul>
                    <li>The SCforH approach is intended for use in all sports clubs. It can be implemented in any type
                        of sports club, from the small, purely voluntary club to the large club staffed by paid
                        professionals.
                    </li>
                    <li> Clubs also differ in terms of their sporting aims and programmes. The SCforH approach can be
                        applied in any kind of sports club, regardless of its aims and the sport disciplines offered.
                    </li>
                    <li>According to the SCforH approach, health is defined broadly to encompass three main dimensions:
                        physical, mental, and social. The aims of SCforH initiatives in sports clubs can be modified
                        such that they place a focus on any or all
                        of these dimensions of health.</li>
                    <li>The SCforH approach can be applied in practice as a philosophy that guides the way the sports
                        club and/or its activities are run or the way a scheduled project or programme is conducted.
                    </li>
                    <li>Rather than maintaining the SCforH as an independent initiative, the aim should be to ultimately
                        integrate the SCforH approach into the club’s daily activities, such as coaching.</li>
                    <li>The SCforH approach has been designed to serve members of sports clubs and participants in all
                        age groups: children, adolescents, adults, and seniors.</li>
                </ul>
                <ol>
                    <li>The SCforH approach is intended for use in all sports clubs. It can be implemented in any type
                        of sports club, from the small, purely voluntary club to the large club staffed by paid
                        professionals.
                    </li>
                    <li> Clubs also differ in terms of their sporting aims and programmes. The SCforH approach can be
                        applied in any kind of sports club, regardless of its aims and the sport disciplines offered.
                    </li>
                    <li>According to the SCforH approach, health is defined broadly to encompass three main dimensions:
                        physical, mental, and social. The aims of SCforH initiatives in sports clubs can be modified
                        such that they place a focus on any or all
                        of these dimensions of health.</li>
                    <li>The SCforH approach can be applied in practice as a philosophy that guides the way the sports
                        club and/or its activities are run or the way a scheduled project or programme is conducted.
                    </li>
                    <li>Rather than maintaining the SCforH as an independent initiative, the aim should be to ultimately
                        integrate the SCforH approach into the club’s daily activities, such as coaching.</li>
                    <li>The SCforH approach has been designed to serve members of sports clubs and participants in all
                        age groups: children, adolescents, adults, and seniors.</li>
                    </ul>

                    <div class="video-wrapper">
                        <video  controls>
                            <source src="https://www.w3schools.com/html/mov_bbb.mp4" type="video/mp4">
                            <source src="https://www.w3schools.com/html/mov_bbb.ogg" type="video/ogg">
                            Your browser does not support HTML5 video.
                        </video>
                    </div>

                     <!-- INFOGRAPHIC 1 -->
                <div id="guiding-principles">
                    <section class="infographic">
                        <div class="graph-wrap">
                            <div class="papper-holder">
                                <p>Guiding principles of the SCforH approach</p>
                            </div>
                            <ul class="unstyle-list slick-center">
                                <li class="rect-inner rect block-overlay yellow">
                                    <h2>Promotes sport thet are part of the club’s standard programme</h2>
                                    <p>1By definition, health-enhancing sports activities are any sports activities that
                                        are beneficial to health and that present no or only minimal health and safety
                                        risks. By adhering to this principle, you will ensure that the activities
                                        offered at a sports club will help the participants meet the physical activity
                                        recommendations.</p>
                                </li>
                                <li class="rect-inner rect block-overlay yellow">
                                    <h2>It is based on well-established evidence-based practives</h2>
                                    <p>2By definition, health-enhancing sports activities are any sports activities that
                                        are beneficial to health and that present no or only minimal health and safety
                                        risks. By adhering to this principle, you will ensure that the activities
                                        offered at a sports club will help the participants meet the physical activity
                                        recommendations.</p>
                                </li>
                                <li class="rect-inner rect block-overlay yellow">
                                    <h2>Uses qualified and competent personnel</h2>
                                    <p>3By definition, health-enhancing sports activities are any sports activities that
                                        are beneficial to health and that present no or only minimal health and safety
                                        risks. By adhering to this principle, you will ensure that the activities
                                        offered at a sports club will help the participants meet the physical activity
                                        recommendations.</p>
                                </li>
                                <li class="rect-inner rect block-overlay yellow">
                                    <h2>Promotes sport thet are part of the club’s standard programme</h2>
                                    <p>4By definition, health-enhancing sports activities are any sports activities that
                                        are beneficial to health and that present no or only minimal health and safety
                                        risks. By adhering to this principle, you will ensure that the activities
                                        offered at a sports club will help the participants meet the physical activity
                                        recommendations.</p>
                                </li>
                                <li class="rect-inner rect block-overlay yellow">
                                    <h2>Does not pose substantial health and safety risks</h2>
                                    <p>5By definition, health-enhancing sports activities are any sports activities that
                                        are beneficial to health and that present no or only minimal health and safety
                                        risks. By adhering to this principle, you will ensure that the activities
                                        offered at a sports club will help the participants meet the physical activity
                                        recommendations.</p>
                                </li>
                                <li class="rect-inner rect block-overlay yellow">
                                    <h2>Takes place in a ‘healthy’ environment</h2>
                                    <p>6By definition, health-enhancing sports activities are any sports activities that
                                        are beneficial to health and that present no or only minimal health and safety
                                        risks. By adhering to this principle, you will ensure that the activities
                                        offered at a sports club will help the participants meet the physical activity
                                        recommendations.</p>
                                </li>
                                <li class="rect-inner rect block-overlay yellow">
                                    <h2>Involves a commitment to creating an enjoyable social and motivational climate
                                    </h2>
                                    <p>7By definition, health-enhancing sports activities are any sports activities that
                                        are beneficial to health and that present no or only minimal health and safety
                                        risks. By adhering to this principle, you will ensure that the activities
                                        offered at a sports club will help the participants meet the physical activity
                                        recommendations.</p>
                                </li>
                            </ul>
                        </div>
                        <div class="slick-count text-center"></div>
                    </section>
                </div>

                <!-- INFOGRAPHIC 2 - KRUGOVI -->
                <div id="application-model">
                    <section class="infographic">
                        <!-- <h1>Application model</h1> -->
                        <div class="graph-wrap">
                            <div class="middle-cyrcle border yellow cyrcle">

                                <img src="http://scforh.local/wp-content/themes/SCforH_theme/img/app-model-arrows.svg"
                                    alt="Target">

                            </div>
                            <ul class="unstyle-list slick-center">
                                <li class="cyrcle-inner cyrcle yellow block-overlay">
                                    <div>

                                        <img src="http://scforh.local/wp-content/themes/SCforH_theme/img/app-model-point.svg"
                                            alt="Target">
                                        <p>1. Assess the present conditions and goals</p>
                                        <ul class="unstyle-list">
                                            <li>• Identify the support and possibilities for SCforH intiative within the
                                                club and in its environment</li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="cyrcle-inner cyrcle yellow block-overlay">
                                    <div>

                                        <img src="http://scforh.local/wp-content/themes/SCforH_theme/img/app-model-calendar.svg"
                                            alt="Target">

                                        <p>2. Plan</p>
                                        <ul class="unstyle-list">
                                            <li>• Define the target group</li>
                                            <li>• Identify the health potential</li>
                                            <li>• Explore the know-how and material support</li>
                                            <li>• Agree upon the aims and strategy</li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="cyrcle-inner cyrcle yellow block-overlay">
                                    <div>

                                        <img src="http://scforh.local/wp-content/themes/SCforH_theme/img/app-model-checklist.svg"
                                            alt="Target">

                                        <p>3. Implement</p>
                                        <ul class="unstyle-list">
                                            <li>• Inform internally and externally</li>
                                            <li>• Secure competent and supportive personnel</li>
                                            <li>•Monitor the feasibility</li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="cyrcle-inner cyrcle yellow block-overlay">
                                    <div>

                                        <img src="http://scforh.local/wp-content/themes/SCforH_theme/img/app-model-follow-up.svg"
                                            alt="Target">

                                        <p>4. Follow-up</p>
                                        <ul class="unstyle-list">
                                            <li>• Keep records and evaluate</li>
                                            <li>• Share your success</li>
                                            <li>• Go back to the former stage if needed</li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="slick-count text-center"></div>
                    </section>
                </div>

            </div>
        </main>
    </div>

    <div class="next-news__wrap">
        <div class="container">
            <section class="layout__inner next-news__content">
                <h1 class="page-title">News</h1>
                <div class="columns is-variable is-3 is-multiline about-list">

                    <div class="column is-4-tablet ">
                        <a href="#" class="cart-news">
                            <div class="cart-news__img cart-news-row__img--square cover"
                                style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/news1.png)">
                            </div>
                            <div class="cart-news__content">
                                <span class="cart-news__date">August 2019</span>
                                <h4 class="cart-news__title">Sports Clubs for Health chosen as an Erasmus+ Success Story
                                </h4>
                                <button href="#" class="btn-link">Learn more <span
                                        class="arrow arrow--right"></span></button>
                            </div>
                        </a>
                    </div>

                    <div class="column is-4-tablet ">
                        <a href="#" class="cart-news">
                            <div class="cart-news__img cart-news-row__img--square cover"
                                style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/news1.png)">
                            </div>
                            <div class="cart-news__content">
                                <span class="cart-news__date">August 2019</span>
                                <h4 class="cart-news__title">Sports Clubs for Health chosen as an Erasmus+ Success Story
                                </h4>
                                <button href="#" class="btn-link">Learn more <span
                                        class="arrow arrow--right"></span></button>
                            </div>
                        </a>
                    </div>

                    <div class="column is-4-tablet ">
                        <a href="#" class="cart-news">
                            <div class="cart-news__img cart-news-row__img--square cover"
                                style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/news1.png)">
                            </div>
                            <div class="cart-news__content">
                                <span class="cart-news__date">August 2019</span>
                                <h4 class="cart-news__title">Sports Clubs for Health chosen as an Erasmus+ Success Story
                                </h4>
                                <button href="#" class="btn-link">Learn more <span
                                        class="arrow arrow--right"></span></button>
                            </div>
                        </a>
                    </div>

                </div>
            </section>
        </div>
    </div>
</div>

<?php get_footer(); ?>