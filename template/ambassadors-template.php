<?php /* Template Name: Ambassadors */ get_header(); ?>

<main class="ambassadors layout">
    <!-- <div class="nav-fullwidth background-blue">
        <div class="container">
            <a href="/contact/contact-information/">Contact <span class="arrow arrow--right"></span></a>
            <span class="sub-element">SCforH Ambassadors</span>
        </div>
    </div> -->

    <section class="background-yellow section">

        <div class="shape-red-left">
            <img src="<?php echo get_template_directory_uri() ?>/img/red-shape-half-left-oval.png" alt="">
        </div>
        <div class="shape-big-center">
            <img src="<?php echo get_template_directory_uri() ?>/img/yellow-shape-half-big.png" alt="">
        </div>
        <div class="shape-group-right-ambassadors">
            <div class="shape-circle-red">
                <img src="<?php echo get_template_directory_uri() ?>/img/half-red-right.png" alt="">
            </div>
            <div class="shape-circle-white">
                <img src="<?php echo get_template_directory_uri() ?>/img/half-white-right.png" alt="">
            </div>
            <div class="shape-circle-blue">
                <img src="<?php echo get_template_directory_uri() ?>/img/blue-circle-small.png" alt="">
            </div>
        </div>


        <div class="container">
            <div class="columns is-multiline is-variable">

            <?php if ( have_rows( 'ambassadors' ) ) : ?>
                <?php while ( have_rows( 'ambassadors' ) ) : the_row(); ?>
                    <div class="column is-9-tablet is-6-desktop">
                        <div class="cart-contact tilt">
                            <h4 class="cart-contact-name"><?php the_sub_field( 'name' ); ?>
                            <span class="cart-contact-email">
                            <?php if ( have_rows( 'email' ) ) : ?>
                                <?php while ( have_rows( 'email' ) ) : the_row(); ?>
                                    <a href="mailto:<?php the_sub_field( 'email' ); ?>"><?php the_sub_field( 'email' ); ?></a>
                                <?php endwhile; ?>
                            <?php endif; ?>
                            </span>
                            </h4>

                            <div class="cart-contact__bottom">
                                <h5 class="cart-contact-lang"><span>Consultations in:</span><?php the_sub_field( 'consultation_in:_' ); ?></h5>
                                <button class="btn-link">Read more<span class="arrow arrow--right"></span></button>
                            </div>
                            <div class="hide">
                                <?php $image = get_sub_field( 'image' ); ?>
                                <?php if ( $image ) { ?>
                                    <div class="cart-contact-img">
                                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                                    </div>
                                <?php } ?>
                                
                                <div class="cart-contact-bio">
                                    <?php the_sub_field( 'content' ); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
           
            <?php endif; ?>
            
            </div>
        </div>
    </section>

    <div class="modal">
        <div class="modal__content">
            <div class="modal__close"></div>
            <div class="modal__body">
                <div class="modal__profile">
                    <div class="modal__img"></div>
                    <div class="modal__description">
                        <h3 class="modal__profile-name"></h3>
                        <div class="modal__profile-email"></div>
                        <div class="modal__profile-bio"></div>
                        <div class="modal__profile-lang">Consultations in: <span></span></div>
                    </div>
                </div>
            </div>
            <div class="modal__controls">
                <div class="prev">
                    <span class="arrow arrow--left"></span>
                    <h6 class="prev__name"></h6>
                    <div class="prev__lang">Consultations in: <span></span></div>
                </div>
                <div class="next">
                    <span class="arrow arrow--right"></span>
                    <h6 class="next__name"></h6>
                    <div class="next__lang">Consultations in: <span></span></div>
                </div>
            </div>
        </div>
    </div>

</main>

<?php get_footer('white'); ?>