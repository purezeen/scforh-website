<?php /* Template Name: Single news */ get_header(); ?>


<div class="layout layout__full-width news-page">
    <div class="shape-group">
        <div class="shape-circle-red">
            <img src="<?php echo get_template_directory_uri() ?>/img/red-shape-half-red-left.png" alt="">
        </div>
        <div class="shape-circle-white">
            <img src="<?php echo get_template_directory_uri() ?>/img/white-shape-half.png" alt="">
        </div>
        <div class="shape-circle-blue">
            <img src="<?php echo get_template_directory_uri() ?>/img/blue-circle-small2.png" alt="">
        </div>
    </div>

    <div class="shape-big-center">
        <img src="<?php echo get_template_directory_uri() ?>/img/yellow-shape-half-big.png" alt="">
    </div>

    <div class="shape-group-blue-right">
        <div class="shape-circle-blue">
            <img src="<?php echo get_template_directory_uri() ?>/img/blue-shape-half-right.png" alt="">
        </div>
        <div class="shape-circle-white">
            <img src="<?php echo get_template_directory_uri() ?>/img/white-circle-small.png" alt="">
        </div>
    </div>

    <div class="shape-gray-right">
        <img src="<?php echo get_template_directory_uri() ?>/img/gray-shape-half-right.png" alt="">
    </div>

    <div class="container">

        <main class="layout__inner">
            <div class="news-page__content">
                <h3>Sports Clubs for Health chosen as an Erasmus+ Success Story</h3>
                <p>The third international SCforH project entitled Creating Mechanisms for Continuous Implementation of
                    the Sports Club for Health Guidelines in the European Union (SCforH 2020-22) has been launched in
                    February 2020. This 3-year long project
                    is funded by a European Union Erasmus+ Collaborative Partnerships grant of 398.845 Euro, and it
                    includes 17 partner institutions from 13 countries. </p>
                <blockquote>SCforH is an expert-based approach that supports clubs as well as national and regional
                    sport organisations to recognize the health potential of their sports disciplines and organize
                    health-enhancing sports activities in the sports club
                    setting.
                </blockquote>
                <p>Achieving good health is a desirable goal of sports clubs members and society as a whole, regardless
                    of the reasons an individual engages in sports. SCforH encourages sports clubs to review their own
                    activities and sport discipline(s)
                    in the light of their health potential and promote greater participation in health-enhancing sports
                    activities.</p>
                <h6>The SCforH approach is based on the following ideas:</h6>
                <ul>
                    <li>The SCforH approach is intended for use in all sports clubs. It can be implemented in any type
                        of sports club, from the small, purely voluntary club to the large club staffed by paid
                        professionals.</li>
                    <li> Clubs also differ in terms of their sporting aims and programmes. The SCforH approach can be
                        applied in any kind of sports club, regardless of its aims and the sport disciplines offered.
                    </li>
                    <li>According to the SCforH approach, health is defined broadly to encompass three main dimensions:
                        physical, mental, and social. The aims of SCforH initiatives in sports clubs can be modified
                        such that they place a focus on any or all
                        of these dimensions of health.</li>
                    <li>The SCforH approach can be applied in practice as a philosophy that guides the way the sports
                        club and/or its activities are run or the way a scheduled project or programme is conducted.
                    </li>
                    <li>Rather than maintaining the SCforH as an independent initiative, the aim should be to ultimately
                        integrate the SCforH approach into the club’s daily activities, such as coaching.</li>
                    <li>The SCforH approach has been designed to serve members of sports clubs and participants in all
                        age groups: children, adolescents, adults, and seniors.</li>
                </ul>
            </div>
        </main>
    </div>

    <div class="next-news__wrap">
        <div class="container">
            <section class="layout__inner next-news__content">
                <h1 class="page-title">News</h1>
                <div class="columns is-variable is-3 is-multiline about-list">

                    <div class="column is-4-tablet ">
                        <a href="#" class="cart-news">
                            <div class="cart-news__img cart-news-row__img--square cover"
                                style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/news1.png)">
                            </div>
                            <div class="cart-news__content">
                                <span class="cart-news__date">August 2019</span>
                                <h4 class="cart-news__title">Sports Clubs for Health chosen as an Erasmus+ Success Story
                                </h4>
                                <button href="#" class="btn-link">Learn more <span
                                        class="arrow arrow--right"></span></button>
                            </div>
                        </a>
                    </div>

                    <div class="column is-4-tablet ">
                        <a href="#" class="cart-news">
                            <div class="cart-news__img cart-news-row__img--square cover"
                                style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/news1.png)">
                            </div>
                            <div class="cart-news__content">
                                <span class="cart-news__date">August 2019</span>
                                <h4 class="cart-news__title">Sports Clubs for Health chosen as an Erasmus+ Success Story
                                </h4>
                                <button href="#" class="btn-link">Learn more <span
                                        class="arrow arrow--right"></span></button>
                            </div>
                        </a>
                    </div>

                    <div class="column is-4-tablet ">
                        <a href="#" class="cart-news">
                            <div class="cart-news__img cart-news-row__img--square cover"
                                style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/news1.png)">
                            </div>
                            <div class="cart-news__content">
                                <span class="cart-news__date">August 2019</span>
                                <h4 class="cart-news__title">Sports Clubs for Health chosen as an Erasmus+ Success Story
                                </h4>
                                <button href="#" class="btn-link">Learn more <span
                                        class="arrow arrow--right"></span></button>
                            </div>
                        </a>
                    </div>

                </div>
            </section>
        </div>
    </div>
</div>

<?php get_footer(); ?>