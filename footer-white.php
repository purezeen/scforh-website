<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gulp-wordpress
 */

?>

</div><!-- #content -->

<!-- Back to top -->
<div class="back-to-top" title="back to top"><span class="arrow arrow--up"><span></div> 

<footer class="footer footer--white">
    <div class="shape-group-footer">
        <div data-aos="fade-left" class="shape-circle-red">
            <img src="<?php echo get_template_directory_uri() ?>/img/red-shape-half-red-right2.png" alt="">
        </div>
        <div data-aos="zoom-in" data-aos-delay="1200" class="shape-circle-yellow">
            <img src="<?php echo get_template_directory_uri() ?>/img/yellow-circle-small.png" alt="">
        </div>
        <div data-aos="fade" data-aos-delay="600" class="shape-circle-blue">
            <img src="<?php echo get_template_directory_uri() ?>/img/blue-circle-small2.png" alt="">
        </div>
    </div>

    <div class="container footer__inner is-relative">
        <div class="footer--right-background"></div>

        <div class="footer__content">
            <div class="footer__2col">
                <div class="col">
                    <div class="footer__logo">
                        <img src="<?php echo get_template_directory_uri() ?>/img/Logo-SCforH-White.svg" alt="SCforH">
                    </div>
                    <span class="copyright">Copyright © Sports Club for Health (SCforH), 2020. All rights reserved.</span>
                </div>
                <div class="col">
                    <?php if ( have_rows( 'social_links', 'option' ) ) : ?>
                    <ul class="unstyle-list">
                        <?php while ( have_rows( 'social_links', 'option' ) ) : the_row(); ?>
                        <li><a href="<?php the_sub_field( 'link' ); ?>" target="_blank">
                                <?php $icon = get_sub_field( 'icon' ); ?>
                                <?php if ( $icon ) { ?>
                                <img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>" />
                                <?php } ?>
                                <?php the_sub_field( 'name' ); ?></a>
                        </li>
                        <?php endwhile; ?>
                    </ul>
                    <?php endif; ?>
                </div>
            </div>
            <div class="footer__2col">
                <div class="col">
                    <?php
					wp_nav_menu( array(
						'theme_location'    => 'footer1',
						'depth'             => 1,
						'container'         => false,
						'menu_class'        => 'unstyle-list'
						)
					);
					?>
                </div>
                <div class="col">
                    <?php
						wp_nav_menu( array(
							'theme_location'    => 'footer2',
							'depth'             => 1,
							'container'         => false,
							'menu_class'        => 'unstyle-list'
							)
						);
						?>
                </div>
            </div>
        </div>

        <div class="column footer__contact">
            <div class="footer__contact-inner">
                <?php if ( have_rows( 'footer_contact_information', 'option' ) ) : ?>
                <?php while ( have_rows( 'footer_contact_information', 'option' ) ) : the_row(); ?>
                <?php $icon = get_sub_field( 'icon' ); ?>
                <?php if ( $icon ) { ?>
                <div class="icon-medium">
                    <img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>" />
                </div>
                <?php } ?>
                <?php the_sub_field( 'content' ); ?>

                <?php $button = get_sub_field( 'button' ); ?>
                <?php if ( $button ) { ?>
                <a href="<?php echo $button['url']; ?>" class="btn btn--white"><?php echo $button['title']; ?></a>
                <?php } ?>
                <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>

</footer>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>

</html>