<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package gulp-wordpress
 */

get_header(); ?>

<?php
 	$post_ID = get_the_ID();
	$parentpost_id = wp_get_post_parent_id( $post_ID );
?>

<?php if ( get_field( 'sidebar_layout' , $parentpost_id ) == 1 ) { 
	?>
	<?php
	get_template_part( 'template-parts/layout', 'sidebar' );
	?>
	<?php
	get_footer();
} else {
	?>
	<?php
	get_template_part( 'template-parts/layout', 'single' );

	?>
	<?php
	get_footer('white');
} ?>

