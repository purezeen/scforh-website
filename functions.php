<?php
/**
 * gulp-wordpress functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package gulp-wordpress
 */

if ( ! function_exists( 'gulp_wordpress_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function gulp_wordpress_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on gulp-wordpress, use a find and replace
	 * to change 'gulp-wordpress' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'gulp-wordpress', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'menuname' ),
		'footer1' => esc_html__( 'Footer1', 'menuname' ),
		'footer2' => esc_html__( 'Footer2', 'menuname' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'gulp_wordpress_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'gulp_wordpress_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function gulp_wordpress_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'gulp_wordpress_content_width', 640 );
}
add_action( 'after_setup_theme', 'gulp_wordpress_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function gulp_wordpress_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'gulp-wordpress' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'gulp-wordpress' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'gulp_wordpress_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function gulp_wordpress_scripts() {

	wp_enqueue_style( 'gulp-wordpress-style', get_stylesheet_uri() );

	wp_enqueue_style( 'aos-css', get_template_directory_uri() . '/css/aos.css');

	wp_enqueue_style( 'slick-css',  get_template_directory_uri() . '/css/slick.css' );

	wp_enqueue_script( 'slick-js', get_template_directory_uri() . '/js/slick.min.js', array('jquery'), '1', true );
	
	wp_enqueue_script( 'tilt-js', get_template_directory_uri() . '/js/tilt.jquery.js', array('jquery'), '1', true );
	
	wp_enqueue_script( 'aos-js', get_template_directory_uri() . '/js/aos.js', array(), '1.2', true );
	
	wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js', array('jquery'), '1', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'gulp_wordpress_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


//* ======== EXCERTP ======== *//
function custom_excerpt_length( $length ) {
	return 30;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


function new_excerpt_more( $more ) {
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');


//* =========== ACF ============ */

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Options',
		'menu_title'	=> 'Global',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
}

// Adding Previous and Next Post Links
function pagination_nav() {
	global $wp_query;

	if ( $wp_query->max_num_pages > 1 ) { ?>
		 <nav class="pagination" role="navigation">
			  <div class="nav-previous"><?php next_posts_link( '&larr; Older posts' ); ?></div>
			  <div class="nav-next"><?php previous_posts_link( 'Newer posts &rarr;' ); ?></div>
		 </nav>
<?php }
}

// Adding Page Number Pagination 
function pagination_bar() {
    global $wp_query;
 
    $total_pages = $wp_query->max_num_pages;
 
    if ($total_pages > 1) {
        $current_page = max(1, get_query_var('paged'));
 
        echo paginate_links(array(
            'base' => get_pagenum_link(1) . '%_%',
            'format' => '/page/%#%',
            'current' => $current_page,
				'total' => $total_pages,
				'mid_size' => 1,
				'prev_text' => "",
				'next_text' => ""
        ));
    }
}

function pagination_search() {
	global $wp_query;

	$total_pages = $wp_query->max_num_pages;

	if ($total_pages > 1) {
		 $current_page = max(1, get_query_var('paged'));

		 echo paginate_links(array(
				'base'      => @add_query_arg( 'paged', '%#%' ),
			  'format' => '',
			  'current' => $current_page,
			  'total' => $total_pages,
			  'mid_size' => 1,
			  'prev_text' => "",
			  'next_text' => ""
		 ));
	}
}


// ACF Custom Blocks **************************************************************************************
function register_acf_block_types() {
   // register a testimonial block.
   acf_register_block_type(array(
      'name'              => 'testimonial',
      'title'             => __('Testimonial'),
      'description'       => __('A custom testimonial block.'),
      'render_template'   => 'template-parts/blocks/testimonial/testimonial.php',
      'category'          => 'formatting',
      'icon'              => 'admin-comments',
      'keywords'          => array( 'testimonial', 'quote' ),
   ));
}
// Check if function exists and hook into setup.
if( function_exists('acf_register_block_type') ) {
   add_action('acf/init', 'register_acf_block_types');
}

  // Add custom walker to functions.php
  
  class BS_Page_Walker extends Walker_Page {
	public function start_lvl( &$output, $depth = 0, $args = array() ) {
		 $indent = str_repeat("\t", $depth);
		 $output .= "\n$indent<ul class='accordion__content content unstyle-list' role='menu'>\n";
	}

	public function start_el( &$output, $page, $depth = 0, $args = array(), $current_page = 0 ) {
		 if ( $depth ) {
			  $indent = str_repeat( "\t", $depth );
		 } else {
			  $indent = '';
		 }

		 $css_class = array( 'accordion__item', 'accordion__item-' . $page->ID );

		 if ( isset( $args['pages_with_children'][ $page->ID ] ) ) {
			  $css_class[] = 'page_item_has_children';
		 }

		 if ( ! empty( $current_page ) ) {
			  $_current_page = get_post( $current_page );
			  if ( in_array( $page->ID, $_current_page->ancestors ) ) {
					$css_class[] = 'current_page_ancestor';
			  }
			  if ( $page->ID == $current_page ) {
					$css_class[] = 'current_page_item';
			  } elseif ( $_current_page && $page->ID == $_current_page->post_parent ) {
					$css_class[] = 'current_page_parent';
			  }
		 } elseif ( $page->ID == get_option('page_for_posts') ) {
			  $css_class[] = 'current_page_parent';
		 }

		 /**
		  * Filter the list of CSS classes to include with each page item in the list.
		  *
		  * @since 2.8.0
		  *
		  * @see wp_list_pages()
		  *
		  * @param array   $css_class    An array of CSS classes to be applied
		  *                             to each list item.
		  * @param WP_Post $page         Page data object.
		  * @param int     $depth        Depth of page, used for padding.
		  * @param array   $args         An array of arguments.
		  * @param int     $current_page ID of the current page.
		  */
		 $css_classes = implode( ' ', apply_filters( 'page_css_class', $css_class, $page, $depth, $args, $current_page ) );

		 if ( '' === $page->post_title ) {
			  $page->post_title = sprintf( __( '#%d (no title)' ), $page->ID );
		 }

		 $args['link_before'] = empty( $args['link_before'] ) ? '' : $args['link_before'];
		 $args['link_after'] = empty( $args['link_after'] ) ? '' : $args['link_after'];

		 /** This filter is documented in wp-includes/post-template.php */
		 if ( isset( $args['pages_with_children'][ $page->ID ] ) ) {
			  $output .= $indent . sprintf(
						 '<li class="%s"><a href="%s" class="accordion__button" data-toggle="dropdown" role="button" aria-expanded="false">%s%s%s <span class="arrow arrow--right"></span></a>',
						 $css_classes,
						 get_permalink( $page->ID ),
						 $args['link_before'],
						 apply_filters( 'the_title', $page->post_title, $page->ID ),
						 $args['link_after']
					);
		 } else {
			  $output .= $indent . sprintf(
						 '<li class="%s"><a href="%s">%s%s%s</a>',
						 $css_classes,
						 get_permalink( $page->ID ),
						 $args['link_before'],
						 apply_filters( 'the_title', $page->post_title, $page->ID ),
						 $args['link_after']
					);
		 }


		 if ( ! empty( $args['show_date'] ) ) {
			  if ( 'modified' == $args['show_date'] ) {
					$time = $page->post_modified;
			  } else {
					$time = $page->post_date;
			  }

			  $date_format = empty( $args['date_format'] ) ? '' : $args['date_format'];
			  $output .= ' ' . mysql2date( $date_format, $time );
		 }
	}
}



function search_filter($query) {
	if ( !is_admin() && $query->is_main_query() ) {
	  if ($query->is_search) {
		 $query->set('paged', ( get_query_var('paged') ) ? get_query_var('paged') : 1 );
		 $query->set('posts_per_page',9);
	  }
	}
 }

 function aj_modify_posts_per_page($query)
{
    if ($query->is_search()) {
        $query->set('posts_per_page', '3');
    }
}
	add_action( 'pre_get_posts', 'search_filter' );