<!-- Flexible section  -->
<?php if ( have_rows( 'flexible_section' ) ): ?>

	<?php while ( have_rows( 'flexible_section' ) ) : the_row(); ?>

      <!-- Accordion  -->
		<?php if ( get_row_layout() == 'accordion' ) : ?>
			<?php if ( have_rows( 'accordion' ) ) : ?>

            <div class="accordion">
               <?php while ( have_rows( 'accordion' ) ) : the_row(); ?>
                  <div class="accordion__item">
                     <span class="accordion__button"><?php the_sub_field( 'accordion_title' ); ?><span class="icon-expand"></span></span>
                     <?php if ( have_rows( 'acordion_content' ) ): ?>
                        <div class="accordion__content">
                           <!-- Accordion content  -->
                           <?php while ( have_rows( 'acordion_content' ) ) : the_row(); ?>

                                 <?php if ( get_row_layout() == 'full_with' ) : ?>

                                    <?php the_sub_field( 'content_full_with' ); ?>

                                 <?php elseif ( get_row_layout() == 'video' ) : ?>

                                    <?php get_template_part( 'template-parts/flexible-section-components/video' ); ?>
                                 
                                 <?php elseif ( get_row_layout() == 'add_pdf' ) : ?>
                                 
                                    <?php get_template_part( 'template-parts/flexible-section-components/pdf' ); ?>

                                 <?php elseif ( get_row_layout() == 'table' ) : ?>

                                    <?php get_template_part( 'template-parts/flexible-section-components/table' ); ?>

                                 <?php elseif ( get_row_layout() == 'timeline' ) : ?>

                                    <?php get_template_part( 'template-parts/flexible-section-components/timeline' ); ?>
                                 
                                    <?php elseif ( get_row_layout() == 'add_infographic' ) : ?>
                                    
                                       <?php if ( get_sub_field( 'infographic1' ) == 1 ) { 
                                          get_template_part( 'template-parts/flexible-section-components/infographic1' );
                                       } ?>
                                       <?php if ( get_sub_field( 'infographic2' ) == 1 ) { 
                                          get_template_part( 'template-parts/flexible-section-components/infographic2' );
                                       } ?>

                                 <?php endif; ?>
                              <?php endwhile; ?>

                        </div>
                     <?php endif; ?>
                  </div>
               <?php endwhile; ?>
            </div>
			<?php endif; ?>
         <!-- End accordion -->

         <!-- Full content  -->
         <?php elseif ( get_row_layout() == 'full_content' ) : ?>

            <?php the_sub_field( 'content' ); ?>
            <!-- End full content  -->

         <!-- Video section  -->
         <?php elseif ( get_row_layout() == 'video' ) : ?>

            <?php get_template_part( 'template-parts/flexible-section-components/video' ); ?>

         <!-- Pdf section  -->
		   <?php elseif ( get_row_layout() == 'add_pdf' ) : ?>

            <?php get_template_part( 'template-parts/flexible-section-components/pdf' ); ?>
            <!-- End pdf section  -->
         
         <!-- Add table  -->
         <?php elseif ( get_row_layout() == 'table' ) : ?>

            <?php get_template_part( 'template-parts/flexible-section-components/table' ); ?>
      
         <!-- Add timeline  -->
         <?php elseif ( get_row_layout() == 'timeline' ) : ?>

            <?php get_template_part( 'template-parts/flexible-section-components/timeline' ); ?>

            <?php elseif ( get_row_layout() == 'add_infographic' ) : ?>

            <?php if ( get_sub_field( 'infographic1' ) == 1 ) { 
               get_template_part( 'template-parts/flexible-section-components/infographic1' );
            } ?>
            <?php if ( get_sub_field( 'infographic2' ) == 1 ) { 
               get_template_part( 'template-parts/flexible-section-components/infographic2' );
            } ?>

		<?php endif; ?>
	<?php endwhile; ?>
<?php endif; ?>

