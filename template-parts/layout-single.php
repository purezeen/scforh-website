<div class="layout layout__full-width news-page">

  <div class="shape-group">
    <div class="shape-circle-red">
      <img src="<?php echo get_template_directory_uri() ?>/img/red-shape-half-red-left.png" alt="">
    </div>
    <div class="shape-circle-white">
      <img src="<?php echo get_template_directory_uri() ?>/img/white-shape-half.png" alt="">
    </div>
    <div class="shape-circle-blue">
      <img src="<?php echo get_template_directory_uri() ?>/img/blue-circle-small2.png" alt="">
    </div>
  </div>

  <div class="shape-big-center">
    <img src="<?php echo get_template_directory_uri() ?>/img/yellow-shape-half-big.png" alt="">
  </div>

  <div class="shape-group-blue-right">
    <div class="shape-circle-blue">
      <img src="<?php echo get_template_directory_uri() ?>/img/blue-shape-half-right.png" alt="">
    </div>
    <div class="shape-circle-white">
      <img src="<?php echo get_template_directory_uri() ?>/img/white-circle-small.png" alt="">
    </div>
  </div>

  <div class="shape-gray-right">
    <img src="<?php echo get_template_directory_uri() ?>/img/gray-shape-half-right.png" alt="">
  </div>

  <div class="container">

    <?php while ( have_posts() ) : the_post(); ?>

    <main class="layout__inner">

      <div class="news-page__content <?php echo has_post_thumbnail() ? "with-heroimg" : "";?>">

        <!-- Add featured image  -->
        <?php if ( has_post_thumbnail() ) {
                     $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
                     ?>
        <div class="hero-img cover" style="background-image: url(<?php echo $backgroundImg[0]; ?>)"></div>
        <?php
                  } ?>
        <h1><?php the_title(); ?></h1>
        <?php the_content(); ?>

        <!-- Include flexible sections  -->
        <?php get_template_part( 'template-parts/flexible', 'section' ); ?>
        <!-- End flexible section  -->
      </div>
    </main>

    <?php endwhile; ?>

  </div>
</div>