<div class="about-list">

   <?php
   $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
	$args = array(
		'post__not_in' => array(get_the_ID()),
      'paged' => $paged,
		'post_type' => 'post',
		'posts_per_page' => 6,
		'post_status' => 'publish'
	);
	?>

   <?php $wp_query = new WP_Query( $args ); ?>
	<?php if ($wp_query->have_posts()) : ?>
      <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
         <a href="<?php the_permalink(); ?>" class="cart-news">

            <?php 
            if ( has_post_thumbnail() ) {
               $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );
               $backgroundImg = $backgroundImg[0];
            }else {
               $backgroundImg = "";
            }
            ?>

            <div class="cart-news__img cart-news-row__img--square cover"
               style="background-image: url(<?php echo $backgroundImg; ?>)">
            </div>
            <div class="cart-news__content">
               <span class="cart-news__date"><?php echo get_the_date(); ?></span>
               <h4 class="cart-news__title"><?php the_title(); ?></h4>
               <button href="http://scforh.local/single-news/" class="btn-link">Read more <span class="arrow arrow--right"></span></button>
            </div>
         </a>
      <?php endwhile; ?>
   
      <!-- Adding Page Number Pagination  -->
      <div class="pagination">
         <?php pagination_bar(); ?>
      </div>

      <!-- <div class="pagination">
         <a class="prev page-numbers"></a>
         <a class="page-numbers" href="">1</a>
         <span aria-current="page" class="page-numbers current">2</span>
         <a class="page-numbers" href="">3</a>
         <span class="page-numbers dots">…</span>
         <a class="page-numbers" href="">11</a>
         <a class="page-numbers" href="">12</a>
         <a class="next page-numbers" href=""></a>
      </div> -->


   <?php  wp_reset_postdata(); ?>
   <?php endif; ?>

</div>