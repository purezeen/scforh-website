<!-- Timeline -->
<?php if ( have_rows( 'timeline' ) ) : ?>
   <div class="timeline-wrap">
      <div class='timeline'>
         <?php while ( have_rows( 'timeline' ) ) : the_row(); ?>
            <div class='item'>
               <div class="content">
                  <?php the_sub_field( 'text' ); ?>
               </div>
               <div class="line"> </div>
               <div class='date'> <?php the_sub_field( 'year' ); ?></div>
            </div>
         <?php endwhile; ?>
      </div>
   </div>
<?php endif; ?>
