<table>
   <?php if ( have_rows( 'table_head' ) ) : ?>
      <thead>
         <tr>
            <?php while ( have_rows( 'table_head' ) ) : the_row(); ?>
               <th><?php the_sub_field( 'text' ); ?> </th>
            <?php endwhile; ?>
         </tr>
      </thead>
   <?php endif; ?>

   <?php if ( have_rows( 'table_row' ) ) : ?>
      <tbody>
         <?php while ( have_rows( 'table_row' ) ) : the_row(); ?>
            <tr>
               <?php if ( have_rows( 'text' ) ) : ?>
                  <?php while ( have_rows( 'text' ) ) : the_row(); ?>
                     <td><?php the_sub_field( 'text' ); ?></td>
                  <?php endwhile; ?>
               <?php endif; ?>
            </tr>
         <?php endwhile; ?>   
      </tbody>
   <?php endif; ?>
</table>