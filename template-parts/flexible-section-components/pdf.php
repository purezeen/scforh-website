<!-- Pdf  -->

<?php if ( get_sub_field( 'add pdf' ) ) { ?>
   <a href="<?php the_sub_field( 'add pdf' ); ?>" class="download-file" target="_blank" title="Download">
      <?php the_sub_field( 'pdf_title' ); ?>
   </a>
<?php } ?>