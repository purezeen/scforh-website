<!-- INFOGRAPHIC 1 -->
<div id="guiding-principles">
   <section class="infographic">
      <div class="graph-wrap">
         <div class="papper-holder">
            <p>Guiding principles of the SCforH approach</p>
         </div>
         <ul class="unstyle-list slick-center">
            <li class="rect-inner rect block-overlay yellow">
               <h2>Promotes health-enhancing sports activities</h2>
               <p>By definition, health-enhancing sports activities are any sports activities that are beneficial to health and that present no or only minimal health and safety risks. By adhering to this principle, you will ensure that the activities offered at a sports club will help the participants meet the physical activity recommendations.</p>
            </li>
            <li class="rect-inner rect block-overlay yellow">
               <h2>It is based on well-established evidence-based practices</h2>
               <p>To ensure its effectiveness and minimize health risks to the participants, the SCforH initiative should be grounded in well-established, evidence-based physical activity and sports promotion practices.</p>
            </li>
            <li class="rect-inner rect block-overlay yellow">
               <h2>Uses qualified and competent personnel</h2>
               <p>The SCforH initiatives should be implemented by competent and qualified personnel to make sure they are delivered according to the best practices and ensure the safety of the participants.</p>
            </li>
            <li class="rect-inner rect block-overlay yellow">
               <h2>Promotes sports that are part of the club’s standard programmee</h2>
               <p>Adhering to this principle will allow the SCforH initiative to build upon and utilize the sports club’s existing resources such as premises, equipment, and personnel, most efficiently. Second, it will ensure that the SCforH initiative does not encroach upon the activities offered by other sports clubs in the locality.</p>
            </li>
            <li class="rect-inner rect block-overlay yellow">
               <h2>Does not pose substantial health and safety risks</h2>
               <p>The SCforH initiative should include the use of evidence-based strategies to prevent physical injury, psychological trauma, or any other adverse health outcome. These prevention measures need to be tailored to address specific characteristics of the given sport and participant groups.</p>
            </li>
            <li class="rect-inner rect block-overlay yellow">
               <h2>Takes place in a ‘healthy’ environment</h2>
               <p>To support the adoption of a healthy lifestyle, even beyond the scope of physical activity promotion, those in charge of the implementation of the SCforH approach should assure that their sports club is free of potentially ‘unhealthy’ sponsorship and marketing campaigns.</p>
            </li>
            <li class="rect-inner rect block-overlay yellow">
               <h2>Involves a commitment to creating an enjoyable social and motivational climate
               </h2>
               <p>One of the key pillars of the SCforH approach is that the club is committed to exposing its participants to positive experiences by fostering quality motivation and providing a positive and safe social environment.</p>
            </li>
         </ul>
      </div>
      <div class="slick-count text-center"></div>
   </section>
</div>