<!-- INFOGRAPHIC 2  -->
<div id="application-model">
   <section class="infographic">
      <!-- <h1>Application model</h1> -->
      <div class="graph-wrap">
            <div class="middle-cyrcle border yellow cyrcle">

               <img src="<?php echo get_template_directory_uri(); ?>/img/app-model-arrows.svg"
                  alt="Target">

            </div>
            <ul class="unstyle-list slick-center">
               <li class="cyrcle-inner cyrcle yellow block-overlay">
                  <div>

                        <img src="<?php echo get_template_directory_uri(); ?>/img/app-model-point.svg"
                           alt="Target">
                        <p>1. Assess the present conditions and goals</p>
                        <ul class="unstyle-list">
                           <li>• Identify the support and possibilities for SCforH intiative within the
                              club and in its environment</li>
                        </ul>
                  </div>
               </li>
               <li class="cyrcle-inner cyrcle yellow block-overlay">
                  <div>

                        <img src="<?php echo get_template_directory_uri(); ?>/img/app-model-calendar.svg"
                           alt="Target">

                        <p>2. Plan</p>
                        <ul class="unstyle-list">
                           <li>• Define the target group</li>
                           <li>• Identify the health potential</li>
                           <li>• Explore the know-how and material support</li>
                           <li>• Agree upon the aims and strategy</li>
                        </ul>
                  </div>
               </li>
               <li class="cyrcle-inner cyrcle yellow block-overlay">
                  <div>

                        <img src="<?php echo get_template_directory_uri(); ?>/img/app-model-checklist.svg"
                           alt="Target">

                        <p>3. Implement</p>
                        <ul class="unstyle-list">
                           <li>• Inform internally and externally</li>
                           <li>• Secure competent and supportive personnel</li>
                           <li>•Monitor the feasibility</li>
                        </ul>
                  </div>
               </li>
               <li class="cyrcle-inner cyrcle yellow block-overlay">
                  <div>

                        <img src="<?php echo get_template_directory_uri(); ?>/img/app-model-follow-up.svg"
                           alt="Target">

                        <p>4. Follow-up</p>
                        <ul class="unstyle-list">
                           <li>• Keep records and evaluate</li>
                           <li>• Share your success</li>
                           <li>• Go back to the former stage if needed</li>
                        </ul>
                  </div>
               </li>
            </ul>
      </div>
      <div class="slick-count text-center"></div>
   </section>
</div>