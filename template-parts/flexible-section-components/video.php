<?php if ( get_sub_field( 'add_video' ) == 1 ) { 
   ?>

<div class="video-wrapper video-wrap">

   <img class="video-icon" src="<?php echo get_template_directory_uri()?>/img/video-icon.png" alt="arrow">

   <?php $video_thumbnail2 = get_sub_field( 'video_thumbnail2' ); ?>
   <?php if ( $video_thumbnail2 ) { ?>
   <div class="video-poster cover" style="background-image: url(<?php echo $video_thumbnail2['url']; ?>)"></div>
   <?php } ?>
   <?php $video2 = get_sub_field( 'video2' ); ?>
   <?php if ( $video2 ) { ?>
   <video controls>
      <source src="<?php echo $video2['url']; ?>" type="video/mp4">
      <source src="<?php echo $video2['url']; ?>" type="video/ogg">
      Your browser does not support HTML5 video.
   </video>
   <?php } ?>
</div>

<?php } else { ?>

   <div class="video-wrap">

      <img class="video-icon" src="<?php echo get_template_directory_uri()?>/img/video-icon.png" alt="arrow">

      <?php 
      $video = get_sub_field( 'video' );

      preg_match('/src="(.+?)"/', $video, $matches_url );
      $src = $matches_url[1];	

      preg_match('/embed(.*?)?feature/', $src, $matches_id );
      $id = $matches_id[1];
      $id = str_replace( str_split( '?/' ), '', $id );
      ?>

      <?php $video_thumbnail = get_sub_field( 'video_thumbnail' ); ?>
      <?php if ( $video_thumbnail ) { ?>
      <div class="video-poster cover" style="background-image: url(<?php echo $video_thumbnail['url']; ?>)"></div>
      <?php }else {
      ?>
      <div class="video-poster cover"
         style="background-image: url(http://i3.ytimg.com/vi/<?php echo $id; ?>/hqdefault.jpg)"></div>
      <?php } ?>
      <?php the_sub_field( 'video' ); ?>
   </div>
<?php
} ?>