<?php  if ($post->post_parent)  {
    $ancestors=get_post_ancestors($post->ID);
    $root=count($ancestors)-1;
    $parent = $ancestors[$root];
} else {
    $parent = $post->ID;
} 

if ( 0 == $post->post_parent ) {
    $parents_name = the_title();
} else {
    $parents = get_post_ancestors( $post->ID );
    $parents_name =  apply_filters( "the_title", get_the_title( end ( $parents ) ) );
}
?>

<div class="layout about-page">
    <div class="shape-group">
        <div class="shape-circle-red">
            <img src="<?php echo get_template_directory_uri() ?>/img/red-shape-half-red-left.png" alt="">
        </div>
        <div class="shape-circle-white">
            <img src="<?php echo get_template_directory_uri() ?>/img/white-shape-half.png" alt="">
        </div>
        <div class="shape-circle-blue">
            <img src="<?php echo get_template_directory_uri() ?>/img/blue-circle-small2.png" alt="">
        </div>
    </div>
    <div class="container">
        <div class="columns is-multiline is-desktop">
            <aside class="column is-4-desktop layout__sidebar">
                <div class="layout__sidebar--background"></div>
                <div class="layout__sidebar__nav-list">
                    <div class="layout__sidebar__nav-list-inner">
                        
                        <ul class="accordion__sidebar unstyle-list">
                            <?php //this will be where you add your statement if we're on a parent/child page
                            wp_list_pages(array(
                                'title_li' => '',
                                'child_of' => $parent,
                                'walker' => new BS_Page_Walker(),
                            ));
                            ?>
                        </ul>

                    </div>
                </div>
                <div class="layout__sidebar__nav-tab">
                    <h5><?php echo $parents_name; ?></h5>
                    <span class="arrow arrow--up"></span>
                </div>
            </aside>

            <main class="column is-8-desktop layout__main">
                
                <?php while ( have_posts() ) : the_post();?>
                    <h1><?php the_title(); ?></h1>
                    <?php
                  the_content();
                endwhile; ?>

                <!-- Include flexible sections  -->
               <?php get_template_part( 'template-parts/flexible', 'section' ); ?>
               <!-- End flexible section  -->

            </main>
        </div>
    </div>
</div>

