<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package gulp-wordpress
 */

?>

<a href="<?php echo get_permalink(); ?>" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php 
		if (has_post_thumbnail()) {
			echo the_post_thumbnail( 'medium');
		}
	?>
		<h2><?php the_title(); ?></h2>
		<?php the_content(); ?>
		<p><?php echo get_the_excerpt(); ?></p>
</a><!-- #post-## -->
