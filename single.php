<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package gulp-wordpress
 */

get_header(); ?>

<div class="layout layout__full-width news-page">
   <div class="container">

		<?php while ( have_posts() ) : the_post(); ?>

			<main class="layout__inner">

					<div class="news-page__content <?php echo get_field( 'add_cover_image' ) == 1 ? "with-heroimg" : "";?>">

					<?php if ( get_field( 'add_cover_image' ) == 1 ) {  ?>
						<?php $image = get_field( 'image' ); ?>
						<?php if ( $image ) { ?>
							<div class="hero-img cover" style="background-image: url(<?php echo $image['url']; ?>)"></div>
						<?php }else {
							?>
							<!-- Add featured image  -->
							<?php if ( has_post_thumbnail() ) {
								$backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
							?>
							<div class="hero-img cover" style="background-image: url(<?php echo $backgroundImg[0]; ?>)"></div>
							<?php
						} ?>
							<?php
						} ?>
						<?php
						} else { 
						?>
						<?php
						} ?>

						<h1><?php the_title(); ?></h1>
						<?php the_content(); ?>

						 <!-- Include flexible sections  -->
						 <?php get_template_part( 'template-parts/flexible', 'section' ); ?>
               	<!-- End flexible section  -->
					</div>
			</main>

		<?php endwhile; ?>

   </div>

	<?php
	$args = array(
		'post__not_in' => array(get_the_ID()),
		'post_type' => 'post',
		'posts_per_page' => 3,
		'post_status' => 'publish'
	);
	?>

	<?php $the_query = new WP_Query( $args ); ?>
	<?php if ($the_query->have_posts()) : ?>
		<div class="next-news__wrap">
			<div class="container">
					<section class="layout__inner next-news__content">
						<h1 class="page-title">News</h1>
						<div class="columns is-variable is-3 is-multiline about-list">

							<?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
								<div class="column is-4-tablet ">
									<a href="<?php the_permalink(); ?>" class="cart-news">
										<?php 
										if ( has_post_thumbnail() ) {
											$backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );
											$backgroundImg = $backgroundImg[0];
										}else {
											$backgroundImg = "";
										}
										?>
										<div class="cart-news__img cart-news-row__img--square cover"
											style="background-image: url(<?php echo $backgroundImg; ?>)">
										</div>
										<div class="cart-news__content">
											<span class="cart-news__date"><?php echo get_the_date(); ?></span>
											<h4 class="cart-news__title"><?php the_title(); ?></h4>
											<button class="btn-link">Learn more <span class="arrow arrow--right"></span></button>
										</div>
									</a>
								</div>
							<?php endwhile; ?>
						</div>
				</section>
			</div>
		</div>
	<?php endif; ?>
</div>

<?php get_footer();
