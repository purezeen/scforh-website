<form role="search" method="get" id="searchform" class="searchform searchform-collapse" action="<?php echo esc_url( home_url( '/' ) ); ?>">
   <label class="searchform__wrap" for="s">
      <input class="searchform__input" type="text" placeholder="Search" value="<?php echo get_search_query(); ?>" name="s" id="s" />
      <!-- <span class="searchform__button"></span> -->
     <input class="searchform__button" type="submit" id="searchsubmit" value="" />
   </label>
</form>

