(function($) {

    document.addEventListener('DOMContentLoaded', () => {

        // Get all "navbar-burger" elements
        const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

        // Check if there are any navbar burgers
        if ($navbarBurgers.length > 0) {

            // Add a click event on each of them
            $navbarBurgers.forEach(el => {
                el.addEventListener('click', () => {

                    // Get the target from the "data-target" attribute
                    const target = el.dataset.target;
                    const $target = document.getElementById(target);

                    // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
                    el.classList.toggle('is-active');
                    $target.classList.toggle('is-active');

                });
            });
        }
    });

    $(document).ready(function() {
        //  Sidebar fixed on mobile 
        $('.layout__sidebar').on('click', '.layout__sidebar__nav-tab', function(e) {
            $(this).parent('.layout__sidebar').toggleClass('active');

            $('body').toggleClass('disable-scroll');
        })

        $(".navbar__burger").click(function() {
            $(".navbar").toggleClass("active");
            $("body").toggleClass("disable-scroll");
        });

        if ($(window).width() < 990) {

            $('.menu-item-has-children > a').on("click", function(e) {
                $(this).next('.sub-menu').slideToggle();
                e.stopPropagation();
                e.preventDefault();
            });

            // $('.menu-item-has-children li:not(.menu-item-has-children) > a').on("click", function(e) {
            //     e.stopPropagation();
            //     e.preventDefault();
            // });

        } else {

            // $('.menu-item-has-children').hover(
            //     function() {
            //         $(this).children('.sub-menu').slideDown(500);
            //     },
            //     function() {
            //         $(this).children('.sub-menu').slideUp(500);
            //     })

            $('.menu-item-has-children').hover(
                function() { $(this).children('.sub-menu').addClass('hover'); },
                function() { $(this).children('.sub-menu').removeClass('hover'); }

            )

            $('navbar__list > .menu-item-has-children > a').on("click", function(e) {
                e.stopPropagation();
                e.preventDefault();
            });


        }

        // -----------------------------STICKY NAVBAR
        if ($(window).width() < 990) {
            //----------------- MOBILE STICKY NAVBAR
            var prev = 0;
            var $window = $(window);
            var navMobile = $('.navbar');

            $window.on('scroll', function() {
                var scrollTop = $window.scrollTop();
                navMobile.toggleClass('sticky', scrollTop > prev);
                prev = scrollTop;
            });

        } else {
            //---------------- DESKTOP STICKY NAVBAR
            //    var prev = 0;
            var $window = $(window);
            var nav = $('.navbar');

            $window.on('scroll', function() {
                var scrollTop = $window.scrollTop();
                // var navTop = nav.scrollTop();
                nav.toggleClass('sticky-navbar', scrollTop > 150);
                // prev = scrollTop;
            });
        }


        // ----------------------------------ACCORDION

        $(".accordion__sidebar .page_item_has_children.current_page_item .accordion__button").addClass('active').find('span').addClass("up");
        $(".accordion__sidebar .page_item_has_children.current_page_item ").find(".accordion__content").show();

        $(".accordion__sidebar .page_item_has_children.current_page_parent .accordion__button").addClass('active').find('span').addClass("up");
        $(".accordion__sidebar .page_item_has_children.current_page_parent ").find(".accordion__content").show();

        $(".accordion__sidebar .page_item_has_children  .accordion__button, .accordion .accordion__button").on("click", function(e) {
            e.preventDefault();
            if ($(this).hasClass("active")) {
                $(this).removeClass("active");
                $(this)
                    .siblings(".accordion__content")
                    .slideUp(200);

                $(this).find("span").removeClass("up").addClass("down");

                // $(".accordion__button span")
                //     .removeClass("up")
                //     .addClass("down");

            } else {
                // $(".accordion__button span")
                //     .removeClass("up")
                //     .addClass("down");
                $(this)
                    .find("span")
                    .removeClass("down")
                    .addClass("up");

                // $(".accordion__button").removeClass("active");
                $(this).addClass("active");
                // $(".accordion__content").slideUp(200);
                $(this)
                    .siblings(".accordion__content")
                    .slideDown(200);
            }
        });


        // VIDEO SECTION ---------------------------------------
        $('.video-icon').click(function(e) {
            $(this).hide();
            $(this).next('.video-poster').fadeOut();
            var video = $(this).siblings('iframe')
            var src = video.attr('src');
            video.attr('src', src + '&autoplay=1');
            var video2 = $(this).siblings('video')[0];
            if (video2.paused) {
                video2.play();
            }

            e.preventDefault();
        })


        // ----------------------------------- AMBASSADOR MODAL BOX
        var openProfile = null;
        $('.cart-contact').click(function(event) {
            var profile_name = $(this).find(".cart-contact-name").clone().children().remove().end().text();
            $('.modal__profile-name').html(profile_name);
            var profile_email = $(this).find(".cart-contact-email").html();
            $('.modal__profile-email').html(profile_email);
            var profile_lang = $(this).find(".cart-contact-lang").clone().children().remove().end().text();
            $('.modal__profile-lang span').html(profile_lang);
            var profile_bio = $(this).find(".cart-contact-bio").html();
            $('.modal__profile-bio').html(profile_bio);
            var profile_img = $(this).find(".cart-contact-img").html();
            $('.modal__img').html(profile_img);
            openProfile = $(this);
            var cards = openProfile.parents('.columns').find(".cart-contact");
            var length_cards = cards.length;
            var currentCardIndex = cards.index(openProfile);
            var next = cards.get(currentCardIndex + 1);
            var prev = cards.get(currentCardIndex - 1);
            if (length_cards > currentCardIndex) {
                $(".next").css("display", "block");
                var next_name = $(next).find(".cart-contact-name").clone().children().remove().end().text();
                $('.next__name').html(next_name);
                var next_lang = $(next).find(".cart-contact-lang").clone().children().remove().end().text();
                $('.next__lang span').html(next_lang);
            }

            if (0 < currentCardIndex) {
                $(".prev").css("display", "block");
                var prev_name = $(prev).find(".cart-contact-name").clone().children().remove().end().text();

                $('.prev__name').html(prev_name);
                var prev_lang = $(prev).find(".cart-contact-lang").clone().children().remove().end().text();
                $('.prev__lang span').html(prev_lang);
            }
            if (0 === currentCardIndex) {
                $(".prev").css("display", "none");
            }
            if (length_cards === (currentCardIndex + 1)) {
                $(".next").css("display", "none");
            }

        });

        $(".modal__controls .next").click(function(event) {
            var cards = openProfile.parents('.columns').find(".cart-contact");
            var currentCardIndex = cards.index(openProfile);
            if (cards.length > (currentCardIndex + 1)) {
                cards.get(currentCardIndex + 1).click();
                $(".modal__body").animate({ opacity: '0' }, 0);
                $(".modal__body").animate({ "left": "+=40px" }, "slow");
                $(".modal__body").animate({ "left": "0", opacity: '1' }, "slow");
            } else {
                event.preventDefault();
            }
        });

        $(".modal__controls .prev").click(function(event) {
            var cards = openProfile.parents('.columns').find(".cart-contact");
            var currentCardIndex = cards.index(openProfile);
            if (currentCardIndex > 0) {
                cards.get(currentCardIndex - 1).click();
                $(".modal__body").animate({ opacity: '0' }, 0);
                $(".modal__body").animate({ "left": "-=40px" }, "slow");
                $(".modal__body").animate({ "left": "0", opacity: '1' }, "slow");
            } else {
                event.preventDefault();
            }
        });


        // -----------------------------------MODAL - OPEN/CLOSE
        var cart = $('.cart-contact');
        var modal = $('.modal');
        var close = $('.modal__close');
        var body = $('body');
        cart.on("click", function(e) {
            e.preventDefault();
            modal.addClass('open');
            body.addClass('disable-scroll');
        });
        close.on("click", function() {
            modal.removeClass('open');
            modal.removeClass('open');
            if (body.hasClass('disable-scroll')) {
                body.removeClass('disable-scroll');
            }
        });
        $(window).on('click', function(event) {
            if ($(event.target).hasClass('modal')) {
                modal.removeClass('open');
                body.removeClass('disable-scroll');
            }
        });


        //------------------------------------------------------CONTACT FORM ANIMATION
        $('.contact form input:not([type=submit]), .contact form textarea').focus(function() {
            $(this).parent().parent().addClass('label-anima');
            $('.contact form input:not([type=submit]), .contact form textarea').focusout(function() {
                if ($(this).val() == '') {
                    $(this).parent().parent().removeClass('label-anima');
                } else {
                    $(this).parent().parent().addClass('label-anima');
                }
            })
        });


        $('.contact form input:not([type=submit])').filter(function() {
            return this.value;
        }).parent().parent().addClass('label-anima');

        $('.contact form textarea').filter(function() {
            return this.value;
        }).parent().parent().addClass('label-anima');



        //------------------------------------------APPLICATION MODEL / GUIDING PRINCIPLES
        let $mobile = $(window).width() <= 989;
        // Slick Functionality
        $('.slick-center').on('swipe', function(event, slick, direction, index, currentSlide) {
            var $currentSlideObj = $('.slick-slide.slick-current');
            $currentSlideObj.addClass('seen');
            var currentSlide = $(this).slick('slickCurrentSlide');
        });

        var $statusApp = $('#application-model .slick-count');
        var $statusGuide = $('#guiding-principles .slick-count');
        $('#application-model .slick-center').on('init reInit afterChange', function(event, slick, currentSlide, nextSlide) {
            var i = (currentSlide ? currentSlide : 0) + 1;
            var $slideCount = slick.slideCount;
            if (i <= 9) i = '0' + i;
            if ($slideCount <= 9) $slideCount = '0' + $slideCount;
            $statusApp.html(i + '<span>&nbsp;&nbsp;/&nbsp;' + $slideCount + '</span>');

        });
        $('#guiding-principles .slick-center').on('init reInit afterChange', function(event, slick, currentSlide, nextSlide) {
            var i = (currentSlide ? currentSlide : 0) + 1;
            var $slideCount = slick.slideCount;
            if (i <= 9) i = '0' + i;
            if ($slideCount <= 9) $slideCount = '0' + $slideCount;
            $statusGuide.html(i + '<span>&nbsp;&nbsp;/&nbsp;' + $slideCount + '</span>');

        });

        $('.slick-center').slick({
            mobileFirst: true,
            centerMode: true,
            centerPadding: '60px',
            slidesToShow: 1,
            arrows: false,
            infinite: false,
            variableWidth: true,
            settings: "slick",
            responsive: [{
                breakpoint: 989,
                settings: "unslick"
            }]
        });



        $('.cyrcle-inner, .rect-inner').on('click tap', function(event) {
            event.preventDefault();
            $(this).addClass('seen');
        });


        if (!$mobile) {
            $('#guiding-principles .rect-inner').on('click tap', function(event) {
                event.preventDefault();
                $(this).addClass('clicked');
                $('#guiding-principles .rect-inner').removeClass('active');
                $(this).addClass('active');
                var $text = $(this).children('p').text();
                $('.papper-holder p').addClass('simple-text').text($text);
            });
        }


    });

    /**
     * AOS
     * Cool scrolling animations
     **/
    if ($("[data-aos]").length) {
        AOS.init({
            offset: 100,
            duration: 1400,
            disable: 'mobile',
            once: true
        });
    }

    /**
     * TILT
     **/
    if ($(".tilt")[0]) {

        $(".tilt").tilt({
            glare: true,
            maxGlare: 0.4,
            perspective: 1500
        });
    }

    // animation for contact form submit button
    $(".gform_wrapper  input[type='submit']").after("<div class='button-anima'></div>");

    // navigation - bold items on hover
    $(".navbar__list a").each(function() {
        var val = $(this).text();
        $(this).attr('data-text', val);
    });

    $(".accordion__sidebar a").each(function() {
        var val = $(this).text();
        $(this).attr('data-text', val);
    });

    //  iframes - add class on parent element
    var parent_iframe = $('iframe').parent();
    if (!parent_iframe.hasClass("test")) {
        parent_iframe.addClass('video-wrap');
    }

    // back to top button
    $(window).scroll(function() {
        if ($(this).scrollTop() >= 250) {
            $('.back-to-top').stop().slideDown('fast'); // show the button
        } else {
            $('.back-to-top').stop().slideUp('fast'); // hide the button
        }
    });

    $(".back-to-top").on("click", function() {
        $("html, body").animate({
            scrollTop: 0
        }, 500);
        return false;
    });

})(jQuery);