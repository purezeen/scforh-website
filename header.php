<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gulp-wordpress
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
   <meta charset="<?php bloginfo( 'charset' ); ?>">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="profile" href="http://gmpg.org/xfn/11">
   <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
   <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>


   <div id="page" class="site">

      <nav class="navbar">
         <div class="container">
            <div class="navbar__mobile">
               <div class="navbar__brand">
                  <a href="/" class="navbar__logo">
                     <img src="<?php echo get_template_directory_uri()?>/img/SCforH-small.svg" alt="SCforH">
                  </a>

                  <a href="/" class="navbar__logo navbar__logo-partner">
                     <img src="<?php echo get_template_directory_uri()?>/img/logo-erasmus.png" alt="erasmus">
                  </a>
               </div>

               <a role="button" class="navbar__burger">
                  <span></span>
                  <span></span>
                  <span></span>
               </a>
            </div>
            <a href="/" class="navbar__logo-desktop">
               <img class="logo" src="<?php echo get_template_directory_uri()?>/img/Logo-SCforH-Blue.svg" alt="SCforH">
               <img class="logo-white" src="<?php echo get_template_directory_uri()?>/img/Logo-SCforH-White.svg" alt="SCforH">
               <img class="logo-small" src="<?php echo get_template_directory_uri()?>/img/SCforH-small.svg" alt="SCforH">
            </a>
            <div class="navbar__bottom">
               <div class="navbar__top">

               <?php if ( have_rows( 'social_links', 'option' ) ) : ?>
                  <ul class="unstyle-list social-links">
                     <?php while ( have_rows( 'social_links', 'option' ) ) : the_row(); ?>
                        <li><a href="<?php the_sub_field( 'link' ); ?>" target="_blank"><?php the_sub_field( 'name' ); ?></a></li>
                     <?php endwhile; ?>
                  </ul>
               <?php endif; ?>
                  <div class="navbar__top-logo-partner">
                     <img src="<?php echo get_template_directory_uri()?>/img/logo-erasmus.png" alt="erasmus">
                  </div>

               </div>
               <div class="navbar__list-wrap">
                  <?php
                    wp_nav_menu( array(
                        'theme_location'    => 'primary',
                        'depth'             => 3,
                        'container'         => false,
                        'menu_class'        => 'navbar__list unstyle-list'
                        )
                    );
                    ?>
                  <?php get_search_form(); ?>
               </div>
            </div>
         </div>
      </nav>

      <div id="content" class="site-content">