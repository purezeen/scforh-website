<p><?php echo $attributes['text-test']; ?></p>
<h1>custom block</h1>

<?php if ( isset( $attributes['image-test']['url'] ) ) : ?>
    <img src="<?php echo esc_url( $attributes['image-test']['url'] ); ?>" alt="<?php echo esc_attr( $attributes['image-test']['alt'] ); ?>">
<?php endif; ?>